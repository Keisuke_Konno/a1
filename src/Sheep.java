import java.util.Arrays;

public class Sheep {

   enum Animal {sheep, goat};
   
   public static void main (String[] param) {
      // for debugging
      Animal[] animals = new Animal [10];
      int rCount = 0;
      for (int i=0; i < animals.length; i++) {
         if (Math.random() < 0.5) {
            animals[i] = Animal.goat;
            rCount++;
         } else {
            animals[i] = Animal.sheep;
         }
      }

      System.out.println("Goat:"+rCount+" Sheep:"+(animals.length-rCount));
      System.out.println();
      for(int i=0; i<animals.length; i++) System.out.println("Input"+i+":"+animals[i]);
      System.out.println();
      reorder(animals);
      for(int i=0; i<animals.length; i++) System.out.println("Output"+i+":"+animals[i]);

   }
   
   public static void reorder (Animal[] animals) {
      // TODO!!! Your program here

      //count goats
      int count_goat = 0;
      for(int i=0; i<animals.length; i++){
         if(animals[i] == Animal.goat)count_goat++;
      }

      //exception handling
      if(count_goat == 0){
         Arrays.fill(animals,Animal.sheep);
         return;
      }
      if(count_goat == animals.length) return;

      //counting sort
      int i=0;
      while( i < animals.length){
         if(i <= count_goat) animals[i] = Animal.goat;
         if(i >= count_goat)animals[i] = Animal.sheep;
         i++;
      }

   }
}

